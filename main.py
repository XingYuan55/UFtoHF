"""
将国际通用魔方公式转换为汉字的何氏魔方公式
"""
import re
import json
from typing import List


img = json.load(open(r".\Comparative.json", encoding="utf-8"))  # 通用公式转何氏公式对应表

def homo_to_inter(steps: List[str]):
    """
    将以列表形式分割好的通用公式转换为字符串型何氏公式


    通用公式的每一指令只能以：1)字母 2)数字"2" 3)逆时针号"'"
    不能识别的字母将原样打出

    :param steps: 以列表形式分割好的通用公式
    :return: 对应的字符串形的何氏公式
    """


    res = ""  # 何氏结果字符串
    for step in steps:  # 循环列表的每一步指令
        try:  # 防止错误的步骤指令导致报错
            if step[-1] == '2':  # 是 转2次
                res += img[step[:-1]] + " " + img[step[:-1]] + " "
            elif step.islower() and (step not in ['y', "y'"]):  # 是 转两层
                res += img[step.upper()] + "(贰)" + " "
            else:  # 普通
                res += img[step] + " "
        except KeyError:  # 报错了
            res += step + " "  # 原样打出

    return res


def cut_steps(origin: str):
    """
    将通用公式的每一指令分离出，结果汇为一个步骤列表
    """
    pattern = "([A-z]('?)(2?))"  # 通用公式识别正则表达式
    steps = []
    for step in re.findall(pattern, origin):
        steps += [step[0]]
    return steps


def main():
    print(homo_to_inter(cut_steps(input())))


while True:
    main()
